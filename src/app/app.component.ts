import { Component, OnInit } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';

import { APIService } from './API.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  signedIn: boolean;
  user: any;
  greeting: string;
  todos: any[];

  constructor(private amplifyService: AmplifyService, private apiService: APIService) {}

  async ngOnInit() {
    this.apiService.ListTodos().then(evt => {
      this.todos = evt.items;
    });

    this.apiService.OnCreateTodoListener.subscribe( evt => {
      const data = (evt as any).value.data.onCreateTodo;
      this.todos = [...this.todos, data];
    });

    this.amplifyService.authStateChange$
      .subscribe(authState => {
        this.signedIn = authState.state === 'signedIn';
        if (!authState.user) {
          this.user = null;
        } else {
          this.user = authState.user;
          this.greeting = 'Hello' + this.user.username;
        }
      })
  }

  createTodo() {
    this.apiService.CreateTodo({
      name: 'Angular',
      description: 'testing'
    });
  }
}
